#import "AddPersonCommand.h"

@implementation AddPersonCommand
{
  NSUUID *_identifier;
}

- (NSUUID *)identifier
{
  return _identifier;
}

- (instancetype)initWithCoder: (NSCoder *)decoder
{
  self = [super init];
  if (self)
    {
      _identifier = [[decoder decodeObjectForKey: @"identifier"] retain];
      _name = [[decoder decodeObjectForKey: @"name"] retain];
      _email = [[decoder decodeObjectForKey: @"email"] retain];
      _shouldContact = [decoder decodeBoolForKey: @"shouldContact"];
      _willingToPay = [[decoder decodeObjectForKey: @"willingToPay"] retain];
    }
  return self;
}

- (void)encodeWithCoder: (NSCoder *)coder
{
  [coder encodeObject: _identifier forKey: @"identifier"];
  [coder encodeObject: _name forKey: @"name"];
  [coder encodeObject: _email forKey: @"email"];
  [coder encodeBool: _shouldContact forKey: @"shouldContact"];
  [coder encodeObject: _willingToPay forKey: @"willingToPay"];
}

+ (instancetype)commandToAddPersonNamed: (NSString *)name email: (NSString *)email canBeContacted: (BOOL)shouldContact willingToPay: (NSDecimalNumber *)dollarAmount
{
  AddPersonCommand *command = [self new];
  command->_identifier = [NSUUID new];
  command->_name = [name copy];
  command->_email = [email copy];
  command->_shouldContact = shouldContact;
  command->_willingToPay = [dollarAmount retain];
  return [command autorelease];
}

- (void)dealloc
{
  [_identifier release];
  [_name release];
  [_email release];
  [_willingToPay release];
  [super dealloc];
}

@end
