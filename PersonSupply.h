#import <Foundation/Foundation.h>

@class Person;
@interface PersonSupply : NSObject

- (Person *)personWithEmail: (NSString *)emailAddress;
- (Person *)insertedPerson;
- (void)saveChanges;

@end